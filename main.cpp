#include <stdio.h>
#include <string>
#include <vector>
#include "settingsManager.h"
#include "game.h"
#include "MENU_VIEW.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>

float ScreenWidth = 960;
float ScreenHeight = 540;

ALLEGRO_DISPLAY *display = NULL;
settingsManager *settings = new settingsManager();
const char *fontPath;
ALLEGRO_CONFIG *lang;
enum COLORS {
	WHITE, BLACK, GREY, BLUE
};
ALLEGRO_COLOR color[4];

void loadScreen(){
	ALLEGRO_FONT *font = al_load_ttf_font(fontPath, 22, 0);;
	ALLEGRO_BITMAP *background = al_load_bitmap(settings->getBackgroundFile());
	al_draw_scaled_bitmap(background, 0, 0, 1920, 1080, 0, 0, ScreenWidth, ScreenHeight, 0);
	al_draw_text(font, color[WHITE], ScreenWidth / 2, ScreenHeight / 2, ALLEGRO_ALIGN_CENTRE, al_get_config_value(lang, "general", "loading"));
	al_flip_display();
}

void newGame(){
	loadScreen();
	GAME *game = new GAME("maps/map1.map", ScreenWidth, ScreenHeight);
	game->init();
	al_hide_mouse_cursor(display);
	game->start();
	al_show_mouse_cursor(display);
}

void mainMenu(int menuID){
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue(); //Warteliste f�r Aktionen
	al_register_event_source(event_queue, al_get_keyboard_event_source()); //Tastatur als Aktionsquelle hinzuf�gen
	//TODO: Select menu item via mouseclick
	//al_register_event_source(event_queue, al_get_mouse_event_source());

	bool done = false;
	bool redraw = true;
	MENU_VIEW menuView(lang);

	const char *menuName = al_get_config_value(lang, "main menu", "main menu");
	std::vector<const char*> menuPoints = menuView.getMenu(0);
	std::vector <const char*> languages;
	std::vector <const char*> themes;
	int selected = 0;

	ALLEGRO_FONT *font;
	ALLEGRO_FONT *fontTitle;
	ALLEGRO_BITMAP *background = al_load_bitmap(settings->getBackgroundFile());
	ALLEGRO_EVENT ev;
	
	while (!done){
		if (redraw){
			font = al_load_ttf_font(fontPath, (ScreenHeight / (menuPoints.size() + 2)) / 2, 0);
			fontTitle = al_load_ttf_font(fontPath, (ScreenHeight / (menuPoints.size() + 2)), 0);
			al_draw_scaled_bitmap(background, 0, 0, 1920, 1080, 0, 0, ScreenWidth, ScreenHeight, 0);
			al_draw_text(fontTitle, color[WHITE], ScreenWidth / 2, (ScreenHeight / (menuPoints.size() + 2)) / 2.5, ALLEGRO_ALIGN_CENTRE, menuName);
			for (unsigned i = 0; i < menuPoints.size(); i++){
				al_draw_text(font, (selected == i) ? color[BLUE] : color[WHITE], ScreenWidth / 2, (ScreenHeight / (menuPoints.size() + 2)) * (i + 2), ALLEGRO_ALIGN_CENTRE, menuPoints[i]);
			}
			al_flip_display();
			redraw = false;
		}

		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN){
			if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN && selected < menuPoints.size() - 1){
				selected++;
				if (menuID == 3 && selected < themes.size()){
					fontPath = settings->getFontFile(themes[selected]);
					background = al_load_bitmap(settings->getBackgroundFile(themes[selected]));
				}
				redraw = true;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_UP && selected > 0){
				selected--;
				if (menuID == 3 && selected < themes.size()){
					fontPath = settings->getFontFile(themes[selected]);
					background = al_load_bitmap(settings->getBackgroundFile(themes[selected]));
				}
				redraw = true;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_ENTER){
				if (menuID == 0){
					switch (selected){
					case 0:
						newGame();
						break;
					case 1:
						//TODO: Load a game
						break;
					case 2:
						menuID = 1;
						menuPoints = menuView.getMenu(menuID);
						break;
					case 3:
						done = true;
						break;
					}
				}
				else if (menuID == 1){
					switch (selected){
					case 0:
						menuID = 2;
						menuPoints.clear();
						languages = settings->getAvailableLanguages();
						for (unsigned i = 0; i < languages.size(); i++){
							menuPoints.push_back(settings->getLanguage(languages[i]));
						}
						menuPoints.push_back(al_get_config_value(lang, "settings menu", "back"));
						break;
					case 1:
						menuID = 3;
						menuPoints.clear();
						themes = settings->getAvailableThemes();
						for (unsigned i = 0; i < themes.size(); i++){
							menuPoints.push_back(settings->getTheme(themes[i]));
						}
						menuPoints.push_back(al_get_config_value(lang, "settings menu", "back"));
						break;
					case 2:
						menuID = 0;
						menuPoints = menuView.getMenu(menuID);
						break;
					}
				}
				else if (menuID == 2){
					if (selected < languages.size()){
						settings->setDefaultLanguage(languages[selected]);
						lang = al_load_config_file(settings->getLanguageFile());
						menuView.setLanguage(lang);
					}
					menuID = 1;
					menuPoints = menuView.getMenu(menuID);
				}
				else if (menuID == 3){
					if (selected < themes.size()){
						settings->setDefaultTheme(themes[selected]);
					}
					fontPath = settings->getFontFile();
					background = al_load_bitmap(settings->getBackgroundFile());
					menuID = 1;
					menuPoints = menuView.getMenu(menuID);
				}
				menuName = menuView.getMenuName(menuID);
				selected = 0;
				redraw = true;
			}
		}
	}
	al_clear_to_color(color[BLACK]);
	al_flip_display();
}

int main(int argc, char **argv)
{
	//Allegro initalisieren
	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}

	//Fenster �ffnen
	/*al_set_new_display_flags(ALLEGRO_FULLSCREEN);
	ALLEGRO_DISPLAY_MODE   disp_data;
	al_get_display_mode(al_get_num_display_modes() - 1, &disp_data);
	ScreenWidth = disp_data.width;
	ScreenHeight = disp_data.height;*/
	display = al_create_display(ScreenWidth, ScreenHeight);
	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}
	
	//Allegro Plugins initalisieren
	al_init_font_addon();
	al_init_ttf_addon();
	al_init_image_addon();
	al_init_primitives_addon();

	//Tastatur und Maus "installieren"
	al_install_keyboard();
	al_install_mouse();

	//Ein Farben in Array schreiben f�r einfacheren Abruf
	color[WHITE] = al_map_rgb(255, 255, 255);
	color[BLACK] = al_map_rgb(0, 0, 0);
	color[GREY] = al_map_rgb(165, 165, 165);
	color[BLUE] = al_map_rgb(0, 85, 255);

	//Datei mit Sprachdaten laden
	lang = al_load_config_file(settings->getLanguageFile());

	//Standartschriftart setzen
	fontPath = settings->getFontFile();

	//Fensternamen setzen
	al_set_window_title(display, al_get_config_value(lang, "general", "windowtitle"));

	//Starten des Programmes
	mainMenu(0);

	//TODO: Abfragen, ob wirklich beendet werden soll

	al_clear_to_color(al_map_rgb(0, 0, 0));

	al_flip_display();

	al_destroy_display(display);

	return 0;
}