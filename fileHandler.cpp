#include "fileHandler.h"
#include <vector>
#include <string>
#include <allegro5\allegro.h>

ALLEGRO_FILE *file;
char charBuff[1024];

std::vector<int> explodeInt(char string[], char limiter){
	int countLimiter = 0, i = 0;
	char lastChar = limiter;
	std::vector<int> result;
	result.reserve(10);
	while (string[i] != NULL){
		if (lastChar == limiter){
			if (string[i] == limiter) {
				break;
			}
			else{
				result.push_back(string[i] - '0');
			}
		}
		else if (string[i] == limiter){
			countLimiter++;
		}
		else if ((string[i] - '0') <= 9 && (string[i] - '0') >= 0){
			result[countLimiter] = (result[countLimiter] * 10) + (string[i] - '0');
		}
		lastChar = string[i];
		i++;
	}
	return result;
}

fileHandler::fileHandler(char *fileName, char type){
	file = al_fopen(fileName, "r");
}

std::vector <int> fileHandler::getNextDataSetInt(){
	if (al_fgets(file, charBuff, 1024))
		return explodeInt(charBuff, ' ');
	else
		return std::vector < int > { -1 };
}

char *fileHandler::getRawLine(){
	al_fgets(file, charBuff, 1024);
	return charBuff;
}

void fileHandler::destroy(){
	al_fclose(file);
}