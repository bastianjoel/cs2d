#include "Map.h"
#include "fileHandler.h"
#include <vector>

void Map::findStartPoint(){
	for (int y = 0; y < this->size.y; y++){
		for (int x = 0; x < this->size.x; x++){
			if (marks[x][y] == 2){
				this->startPoint.x = x;
				this->startPoint.y = y;
				return;
			}
		}
	}
	startPoint = { 0, 0 };
}

void Map::findSpawnPoints(){ 
	for (int y = 0; y < this->size.y; y++){
		for (int x = 0; x < this->size.x; x++){
			if (marks[x][y] == 3){
				this->spawnPoints.push_back({ x, y });
				printf("(%i|%i) \n", x, y);
			}
		}
	}
}

Map::Map(char* fileName)
{
	fileHandler mapFile(fileName, 'r');

	std::vector<int> row;
	int lastY = 0;
	int sizeY = 0;

	row = mapFile.getNextDataSetInt(); //Get roomsize
	this->size.x = row[0]; this->size.y = row[1];
	map.resize((map.size() < row[0]) ? row[0] : map.size()); //Resize map(x) vector
	marks.resize((map.size() < row[0]) ? row[0] : map.size()); //Resize marks(x) vector

	for (unsigned i = 0; i < map.size(); i++) map[i].resize((map[0].size() < row[1]) ? row[1] : map[0].size()); //Resize map(y) vectors
	for (unsigned i = 0; i < marks.size(); i++) marks[i].resize((map[0].size() < row[1]) ? row[1] : map[0].size()); //Resize marks(y) vectors

	while (true){
		row = mapFile.getNextDataSetInt();
		if (row[0] == -1) break;
		if (row[1] >= lastY){
			if (map.size() > row[0] && map[row[0]].size() > row[1])
				map[row[0]][row[1]] = row[2];
			if (marks.size() > row[0] && marks[row[0]].size() > row[1])
				marks[row[0]][row[1]] = row[3];
		}
		else{
			break;
		}
		lastY = row[1];
	}
	this->findStartPoint(); //Startpunkt finden
	this->findSpawnPoints(); //Spawnpunkte f�r Gegner finden
	mapFile.destroy();
}

int Map::getCoordTextureID(coord loc){
	return map[loc.x][loc.y];
}

int Map::getCoordInfoID(coord loc){
	if (loc.x < (float)marks.size() && loc.y < (float)marks.front().size() &&
		loc.x >= 0 && loc.y >= 0)
		return marks[loc.x][loc.y];
	else
		return 1;
}

float Map::getZoom(float ScreenWidth){
	return ScreenWidth / 960;
}