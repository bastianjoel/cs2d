#pragma once
#include <string>
#include <vector>
#include <allegro5/allegro.h>
class settingsManager
{
public:
	settingsManager();
	std::vector<const char*> getAvailableLanguages();
	std::vector<const char*> getAvailableThemes();
	void setDefaultTheme(const char *theme);
	void setDefaultLanguage(const char *lang);
	const char* getLanguage();
	const char* getTheme();
	const char* getLanguage(const char *lang);
	const char* getTheme(const char *theme);

	//Get File
	const char* getThemePath();
	const char* getThemePath(const char *theme);
	const char* getLanguageFile();
	const char* getBackgroundFile();
	const char* getBackgroundFile(const char *theme);
	const char* getTextureFile();
	const char* getTextureFile(const char *theme);
	const char* getGameBackgroundFile();
	const char* getGameBackgroundFile(const char *theme);
	const char* getPlayerFile();
	const char* getPlayerFile(const char *theme);
	const char* getPointerFile();
	const char* getPointerFile(const char *theme);
	const char* getFontFile();
	const char* getFontFile(const char *theme);
private:
	ALLEGRO_CONFIG *config;
	std::string langPath;
	std::string fontPath;
	std::string themePath;
	std::string backgroundPath;
	std::string texturePath;
};