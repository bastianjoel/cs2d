#pragma once
#include <vector>
#include <allegro5\allegro.h>
class MENU_VIEW
{
public:
	MENU_VIEW(ALLEGRO_CONFIG *langFile);
	const char* getMenuName(int menuID);
	std::vector<const char*> getMenu(int menuID);
	void setLanguage(ALLEGRO_CONFIG *langFile);
private:
};

