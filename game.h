#pragma once
#include <allegro5\allegro.h>
#include "Map.h"
#include "player.h"
#include "settingsManager.h"
class GAME
{
public:
	GAME(char *mapFile, int ScreenWidth, int ScreenHeight);
	void init();
	void start();
private:
	char* mapFile;
	int ScreenWidth;
	int ScreenHeight;
	float zoom;
	Map::coord startPoint;
	Map *map;
	Player *player;
	settingsManager *settings = new settingsManager();
};

