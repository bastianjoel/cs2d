#pragma once
#include <vector>

class fileHandler
{
public:
	fileHandler(char *file, char type);
	std::vector < int > getNextDataSetInt();
	char *getRawLine();
	void destroy();
};

