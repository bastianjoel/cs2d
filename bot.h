#pragma once
#include <vector>
#include "Map.h"

class Bot
{
public:
	Bot(Map *map);
	typedef struct coord{ float x; float y; };
	typedef struct botInfo{ coord pos; };
	typedef struct{
		float x; //Y Koordinate
		float y; //X Koordinate
		int g; //Wert G (Anzahl der durchläufe zum Erreichen dieses punktes
		float h; //Geschätzte Entfernung zum Ausgang
		coord p; //Vorgänger
	}li;
	bool add(float x, float y);
	coord getPos(int id);
	float getAngel(int id, coord player);
	int numBots();
	void findPath(int id, coord player);
private:
	Map *map;
	std::vector<li> getNeighbours(coord point, coord goal, int steps);
	std::vector<botInfo> bots;
	void aStar(coord start, coord target);
	float getH(coord s, coord g);
};

