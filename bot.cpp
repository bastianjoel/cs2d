#include "bot.h"
#include <algorithm>


Bot::Bot(Map *map)
{
	this->map = map;
}

bool Bot::add(float x, float y){ //Bot an einem Punkt hinzuf�gen
	this->bots.push_back({});
	this->bots.back().pos.x = x;
	this->bots.back().pos.y = y;
	printf("Bot added!\n");
	return true;
}

Bot::coord Bot::getPos(int id){ //Position eines Bots zur�ckgeben
	return this->bots[id].pos;
}

float Bot::getAngel(int id, coord player){ //Drehung des Bots um zum Spieler zu zeigen berechnen
	float playerAngel = 0;
	playerAngel = std::atan((player.x - this->bots[id].pos.x) / (this->bots[id].pos.y - player.y));

	if (player.y - this->bots[id].pos.y > 0){
		playerAngel += (float) 3.14159265;
	}
	return playerAngel;
}

void Bot::findPath(int id, coord player){
	this->aStar(this->bots[id].pos, player);
}

bool sortByF(Bot::li i, Bot::li j) {
	float aF = i.h + i.g;
	float bF = j.h + j.g;
	return (aF<bF); //?
}

void Bot::aStar(coord start, coord goal){
	std::vector<li> closedset;
	std::vector<li> openset;
	std::vector<Bot::li> neighbours;
	bool isInList = false;
	openset.push_back({ start.x, start.y, 0, this->getH(start, goal), { -1, -1 } });
	printf("ASTar gestartet! (%f|%f) to (%f|%f)\n", start.x, start.y, goal.x, goal.y);
	while (!openset.empty()){
		std::sort(openset.begin(), openset.end(), [](li a, li b){
			float aF = a.h + a.g;
			float bF = b.h + b.g;

			return (aF < bF);
		});
		closedset.push_back(openset.front());
		openset.erase(openset.begin());

		neighbours = getNeighbours({ closedset.back().x, closedset.back().y }, goal, closedset.back().g);
		//printf("Nachbar bei (%f|%f)!\n", neighbours[0].x, neighbours[0].y);
		//printf("Nachbar bei (%f|%f)!\n", neighbours[1].x, neighbours[1].y);
		//printf("Nachbar bei (%f|%f)!\n", neighbours[2].x, neighbours[2].y);
		//printf("Nachbar bei (%f|%f)!\n", neighbours[3].x, neighbours[3].y);

		for (unsigned i = 0; i < neighbours.size(); i++){
			for (unsigned j = 0; j < closedset.size(); j++){
				if (closedset[j].x == neighbours[i].x && closedset[j].y == neighbours[i].y){
					isInList = true;
				}
			}
			if (!isInList){
				if (neighbours[i].x == goal.x && neighbours[i].y == goal.y){
					printf("Gefunden!\n");
					return;
				}
				for (unsigned j = 0; j < openset.size(); j++){
					if (neighbours[i].x == openset[j].x && neighbours[i].y == openset[j].y){ isInList = true; }

					if (neighbours[i].x == openset[j].x && neighbours[i].y == openset[j].y && openset[j].g > neighbours[i].g){
						openset[j] = neighbours[i];
					}
				}
				if (isInList == false){
					openset.push_back(neighbours[i]);
				}
				else{
					isInList = false;
				}
			}
			else{
				isInList = false;
			}
		}
	}
	printf("Closedset: %i\n", closedset.size());
}

std::vector<Bot::li> Bot::getNeighbours(coord point, coord goal, int steps){
	std::vector<li> neighbours;
	steps++;
	if (point.x - 1 >= 0 && map->getCoordInfoID(Map::coord{ point.x - 1, point.y }) != 1){
		neighbours.push_back({ point.x - 1,	//x
			point.y,						//y
			steps,							//g
			this->getH(coord{ point.x - 1, point.y }, goal), //h
			coord{ point.x, point.y }		//p
		});
	}
	if (map->getCoordInfoID(Map::coord{ point.x + 1, point.y }) != 1){
		neighbours.push_back({ point.x + 1,	//x
			point.y,						//y
			steps,							//g
			this->getH(coord{ point.x + 1, point.y }, goal), //h
			coord{ point.x, point.y }		//p
		});
	}
	if (point.y - 1 >= 0 && map->getCoordInfoID(Map::coord{ point.x, point.y - 1 }) != 1){
		neighbours.push_back({ point.x,		//x
			point.y - 1,					//y
			steps,							//g
			this->getH(coord{ point.x, point.y - 1 }, goal), //h
			coord{ point.x, point.y }		//p
		});
	}
	if (map->getCoordInfoID(Map::coord{ point.x, point.y + 1 }) != 1){
		neighbours.push_back({ point.x,		//x
			point.y + 1,					//y
			steps,							//g
			this->getH(coord{ point.x, point.y + 1 }, goal), //h
			coord{ point.x, point.y }		//p
		});
	}
	return neighbours;
}

float Bot::getH(coord s, coord g){
	float dx = abs(s.x - g.x);
	float dy = abs(s.y - g.y);

	return (float) 1 * (dx * dx + dy * dy);
}

int Bot::numBots(){ return this->bots.size(); } //Anzahl der Bots zur�chgeben
