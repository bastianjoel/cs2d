#include "settingsManager.h"
#include <string>
#include <vector>
#include <allegro5/allegro.h>

settingsManager::settingsManager(){
	config = al_load_config_file("settings.ini");
}
std::vector<const char*> settingsManager::getAvailableLanguages(){
	std::vector<const char*> languages;
	ALLEGRO_CONFIG_ENTRY *iterator = NULL;
	languages.push_back(al_get_first_config_entry(config, "languages", &iterator));
	while (languages.back() != NULL){
		languages.push_back(al_get_next_config_entry(&iterator));
	}
	languages.erase(languages.end() - 1);
	return languages;
}
std::vector<const char*> settingsManager::getAvailableThemes(){
	std::vector<const char*> themes;
	ALLEGRO_CONFIG_ENTRY *iterator = NULL;
	themes.push_back(al_get_first_config_entry(config, "themes", &iterator));
	while (themes.back() != NULL){
		themes.push_back(al_get_next_config_entry(&iterator));
	}
	themes.erase(themes.end() - 1);
	return themes;
}
void settingsManager::setDefaultTheme(const char *theme){
	al_set_config_value(config, "general", "theme", theme);
	al_save_config_file("settings.ini", config);
}
void settingsManager::setDefaultLanguage(const char *lang){
	al_set_config_value(config, "general", "lang", lang);
	al_save_config_file("settings.ini", config);
}
const char* settingsManager::getLanguage(){
	return al_get_config_value(config, "general", "lang");
}
const char* settingsManager::getTheme(){
	return al_get_config_value(config, "general", "theme");
}
const char* settingsManager::getLanguage(const char *lang){
	return al_get_config_value(config, "languages", lang);
}
const char* settingsManager::getTheme(const char *theme){
	return al_get_config_value(config, "themes", theme);
}

//Get File
const char* settingsManager::getThemePath(){
	themePath = "themes/";
	themePath += getTheme();
	return themePath.c_str();
}
const char* settingsManager::getThemePath(const char *theme){
	themePath = "themes/";
	themePath += theme;
	return themePath.c_str();
}
const char* settingsManager::getLanguageFile(){
	langPath = "lang/";
	langPath += this->getLanguage();
	langPath += ".ini";
	return langPath.c_str();
}
const char* settingsManager::getBackgroundFile(){
	backgroundPath = getThemePath();
	backgroundPath += "/menubg.jpg";
	return backgroundPath.c_str();
}
const char* settingsManager::getBackgroundFile(const char *theme){
	backgroundPath = getThemePath(theme);
	backgroundPath += "/menubg.jpg";
	return backgroundPath.c_str();
}
const char* settingsManager::getTextureFile(){
	texturePath = getThemePath();
	texturePath += "/texture.bmp";
	return texturePath.c_str();
}
const char* settingsManager::getTextureFile(const char *theme){
	texturePath = getThemePath(theme);
	texturePath += "/texture.bmp";
	return texturePath.c_str();
}
const char* settingsManager::getGameBackgroundFile(){
	texturePath = getThemePath();
	texturePath += "/gamebg.jpg";
	return texturePath.c_str();
}
const char* settingsManager::getGameBackgroundFile(const char *theme){
	texturePath = getThemePath(theme);
	texturePath += "/gamebg.jpg";
	return texturePath.c_str();
}
const char* settingsManager::getPlayerFile(){
	texturePath = getThemePath();
	texturePath += "/player.png";
	return texturePath.c_str();
}
const char* settingsManager::getPlayerFile(const char *theme){
	texturePath = getThemePath(theme);
	texturePath += "/player.png";
	return texturePath.c_str();
}
const char* settingsManager::getPointerFile(){
	texturePath = getThemePath();
	texturePath += "/pointer.png";
	return texturePath.c_str();
}
const char* settingsManager::getPointerFile(const char *theme){
	texturePath = getThemePath(theme);
	texturePath += "/pointer.png";
	return texturePath.c_str();
}
const char* settingsManager::getFontFile(){
	fontPath = getThemePath();
	fontPath += "/font.ttf";
	return fontPath.c_str();
}
const char* settingsManager::getFontFile(const char *theme){
	fontPath = getThemePath(theme);
	fontPath += "/font.ttf";
	return fontPath.c_str();
}