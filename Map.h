#pragma once
#include <vector>
class Map
{
public:
	Map(char* file);
	typedef struct coord{ float x; float y; };
	typedef struct i_coord{ int x; int y; };
	int getCoordTextureID(coord loc);
	int getCoordInfoID(coord loc);
	float getZoom(float ScreenWidth);
	coord size;
	i_coord startPoint;
	std::vector<i_coord> spawnPoints;
private:
	std::vector<std::vector<int>> map;
	std::vector<std::vector<int>> marks;
	void findStartPoint();
	void findSpawnPoints();
};

