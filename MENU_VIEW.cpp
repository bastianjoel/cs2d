#include "MENU_VIEW.h"
#include <allegro5\allegro.h>
#include <vector>

ALLEGRO_CONFIG *langMenu;

MENU_VIEW::MENU_VIEW(ALLEGRO_CONFIG *langFile)
{
	langMenu = langFile;
}

const char* MENU_VIEW::getMenuName(int menuID){
	const char *menu;
	switch (menuID){
	case 1:
		menu = al_get_config_value(langMenu, "settings menu", "settings");
		break;
	case 2:
		menu = al_get_config_value(langMenu, "settings menu", "languages");
		break;
	case 3:
		menu = al_get_config_value(langMenu, "settings menu", "themes");
		break;
	default:
		menu = al_get_config_value(langMenu, "main menu", "main menu");
		break;
	}
	return menu;
}

std::vector<const char*> MENU_VIEW::getMenu(int menuID){
	std::vector<const char*> menuPoints;
	std::vector <const char*> languages;
	switch (menuID){
	case 1:
		menuPoints.push_back(al_get_config_value(langMenu, "settings menu", "language"));
		menuPoints.push_back(al_get_config_value(langMenu, "settings menu", "theme"));
		menuPoints.push_back(al_get_config_value(langMenu, "settings menu", "back"));
		break;
	default:
		menuPoints.push_back(al_get_config_value(langMenu, "main menu", "new game"));
		menuPoints.push_back(al_get_config_value(langMenu, "main menu", "load game"));
		menuPoints.push_back(al_get_config_value(langMenu, "main menu", "settings"));
		menuPoints.push_back(al_get_config_value(langMenu, "main menu", "close"));
		break;
	}
	return menuPoints;
}

void MENU_VIEW::setLanguage(ALLEGRO_CONFIG *langFile){
	langMenu = langFile;
}