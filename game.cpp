#include "game.h"
#include "Map.h"
#include "player.h"
#include "bot.h"
#include "settingsManager.h"
#include <allegro5\allegro.h>
#include <allegro5\allegro_ttf.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_primitives.h>
#include <vector>
#include <math.h>
#define debug false
enum RUN_KEYS {
	CTRL_UP, CTRL_LEFT, CTRL_DOWN, CTRL_RIGHT
};

GAME::GAME(char *mapFile, int ScreenWidth, int ScreenHeight)
{
	GAME::mapFile = mapFile;
	GAME::ScreenWidth = ScreenWidth;
	GAME::ScreenHeight = ScreenHeight;
}

void GAME::init(){
	//GAME::zoom = (float) ScreenWidth / 960;
	GAME::map = new Map(GAME::mapFile);
	GAME::zoom = map->getZoom(ScreenWidth);
	GAME::startPoint = { map->startPoint.x - floor(ScreenWidth / 64 / 2), map->startPoint.y - floor(ScreenHeight / 64 / 2) };
	GAME::player = new Player();
}

void GAME::start(){
	ALLEGRO_FONT *debugFont = al_load_ttf_font("themes/default/debug.ttf", 10, NULL);
	ALLEGRO_BITMAP *background = al_load_bitmap(settings->getGameBackgroundFile());
	ALLEGRO_BITMAP *texture = al_load_bitmap(settings->getTextureFile());
	ALLEGRO_BITMAP *playerImg = al_load_bitmap(settings->getPlayerFile());
	ALLEGRO_BITMAP *mousePointer = al_load_bitmap(settings->getPointerFile());
	ALLEGRO_TIMER *timer = al_create_timer(1.0 / 30);
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue(); //Warteliste f�r Aktionen
	al_register_event_source(event_queue, al_get_mouse_event_source()); //Maus als Aktionsquelle hinzuf�gen
	al_register_event_source(event_queue, al_get_keyboard_event_source()); //Tastatur als Aktionsquelle hinzuf�gen
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	
	al_start_timer(timer);

	bool key[4] = { false, false, false, false };
	bool redraw = true;
	bool spawnNewBots = true;
	Bot::coord botPos;
	float playerAngel = 0;
	Bot *bots = new Bot(map);
	Map::coord mousePos = { ScreenWidth / 2, ScreenHeight / 2 };
	Map::coord playerPos = { ScreenWidth / 2, ScreenHeight / 2 };

	ALLEGRO_COLOR c_black = al_map_rgb(0, 0, 0);

	while (true){
		if (redraw){
			//Draw Background
			al_draw_scaled_bitmap(background, 0, 0, 1920, 1080, 0, 0, ScreenWidth, ScreenHeight, 0);
			//Draw map
			for (int y = 0 + (startPoint.y > 0) ? startPoint.y : 0; y < ((ScreenHeight) / 32 + startPoint.y); y++){
				for (int x = 0 + (startPoint.x > 0) ? startPoint.x : 0; x < ((ScreenWidth) / 32) + startPoint.x; x++){
					if ((map->size.x > x && map->size.y > y) && map->getCoordTextureID({ x, y }) != 0)
						al_draw_scaled_bitmap(texture, (map->getCoordTextureID({ x, y }) % 8) * 32, floor(map->getCoordTextureID({ x, y }) / 8) * 32, 32, 32, (x - startPoint.x) * 64, (y - startPoint.y) * 64, 64, 64, NULL);
				}
			}
			//Draw player
			al_draw_scaled_rotated_bitmap(playerImg, 16, 16, playerPos.x, playerPos.y + 16, 2, 2, playerAngel, NULL);

			//Draw Bots
			for (unsigned i = 0; i < bots->numBots(); i++){
				botPos = bots->getPos(i);
				al_draw_scaled_rotated_bitmap(playerImg, 16, 16, (botPos.x - startPoint.x) * 64, (botPos.y - startPoint.y) * 64 + 16, 2, 2, bots->getAngel(i, { (startPoint.x + floor(playerPos.x / 64)), (startPoint.y + floor(playerPos.y / 64)) }), NULL);
			}

			//Debugging
			if (debug){
				//Mark fields around player
				al_draw_rectangle((floor(floor(playerPos.x) / 64)) * 64, (floor(floor(playerPos.y) / 64) - 1) * 64,
					(floor(floor(playerPos.x) / 64) + 0) * 64 + 64, (floor(floor(playerPos.y) / 64) - 1) * 64 + 64, al_map_rgb(0, 0, 255), 1); //Field upon Player
				al_draw_rectangle((floor(floor(playerPos.x) / 64) + 1) * 64, (floor(floor(playerPos.y) / 64)) * 64,
					(floor(floor(playerPos.x) / 64) + 1) * 64 + 64, (floor(floor(playerPos.y) / 64)) * 64 + 64, al_map_rgb(0, 0, 255), 1); //Field right to Player
				al_draw_rectangle((floor(floor(playerPos.x) / 64)) * 64, (floor(floor(playerPos.y) / 64) + 1) * 64,
					(floor(floor(playerPos.x) / 64) + 0) * 64 + 64, (floor(floor(playerPos.y) / 64) + 1) * 64 + 64, al_map_rgb(0, 0, 255), 1); //Field upon Player
				al_draw_rectangle((floor(floor(playerPos.x) / 64) - 1) * 64, (floor(floor(playerPos.y) / 64)) * 64,
					(floor(floor(playerPos.x) / 64) - 1) * 64 + 64, (floor(floor(playerPos.y) / 64)) * 64 + 64, al_map_rgb(0, 0, 255), 1); //Field left to Player
				al_draw_circle(playerPos.x, playerPos.y + 16, 1, al_map_rgb(255, 0, 0), 1);
				//Draw map Coords
				for (int y = 0; y < ((ScreenHeight) / 32 + startPoint.y); y++){
					for (int x = 0; x < ((ScreenWidth) / 32) + startPoint.x; x++){
						if ((map->size.x > x && map->size.y > y) && map->getCoordTextureID({ x, y }) != 0)
							al_draw_textf(debugFont, c_black, (x - startPoint.x) * 64 + 32, (y - startPoint.y) * 64 + 27, ALLEGRO_ALIGN_CENTRE, "(%i|%i)", x, y);
					}
				}

				//Draw info screen
				float alpha = 0.8f;
				al_draw_filled_rectangle(0, 0, 220, 270, al_map_rgba(255 * alpha, 255 * alpha, 255 * alpha, alpha));
				int infoCount = 1;
				al_draw_textf(debugFont, c_black, 4, infoCount * 12, ALLEGRO_ALIGN_LEFT, "Zoom level: %f", zoom); infoCount++;
				al_draw_textf(debugFont, c_black, 4, infoCount * 12, ALLEGRO_ALIGN_LEFT, "Start Coord: (%i|%i)", map->startPoint.x, map->startPoint.y); infoCount++;
				al_draw_textf(debugFont, c_black, 4, infoCount * 12, ALLEGRO_ALIGN_LEFT, "Player Coord: (%.2f|%.2f)", (startPoint.x + floor(playerPos.x / 64)), (startPoint.y + floor(playerPos.y / 64))); infoCount++;
				al_draw_textf(debugFont, c_black, 4, infoCount * 12, ALLEGRO_ALIGN_LEFT, "Field upon Player: (%.2f|%.2f)", (startPoint.x + floor(playerPos.x / 64)), (startPoint.y + floor(playerPos.y / 64)) - 1); infoCount++;
				al_draw_textf(debugFont, c_black, 4, infoCount * 12, ALLEGRO_ALIGN_LEFT, "Field under Player: (%.2f|%.2f)", (startPoint.x + floor(playerPos.x / 64)), (startPoint.y + floor(playerPos.y / 64)) + 1); infoCount++;
				al_draw_textf(debugFont, c_black, 4, infoCount * 12, ALLEGRO_ALIGN_LEFT, "Field right to Player: (%.2f|%.2f)", (startPoint.x + floor(playerPos.x / 64)) + 1, (startPoint.y + floor(playerPos.y / 64))); infoCount++;
				al_draw_textf(debugFont, c_black, 4, infoCount * 12, ALLEGRO_ALIGN_LEFT, "Field left to Player: (%.2f|%.2f)", (startPoint.x + floor(playerPos.x / 64)) - 1, (startPoint.y + floor(playerPos.y / 64))); infoCount++;
			}
			//Mouse pointer
			al_draw_scaled_bitmap(mousePointer, 0, 0, 50, 50, mousePos.x, mousePos.y, 30, 30, 0);
			redraw = false;
		}
		//Wait for next event
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER) {
			if (key[CTRL_UP]){ //
				if (map->getCoordInfoID({ floor(startPoint.x + floor(playerPos.x / 64)), floor((startPoint.y + floor(playerPos.y / 64))) - ceil(1 * player->getGoSpeed()) }) != 1 &&
					map->getCoordInfoID({ ceil(startPoint.x + floor(playerPos.x / 64)), floor((startPoint.y + floor(playerPos.y / 64))) - ceil(1 * player->getGoSpeed()) }) != 1)
					startPoint.y -= 1 * player->getGoSpeed();
				else
					startPoint.y = floor(startPoint.y);
			}
			else if (key[CTRL_DOWN]){
				if (map->getCoordInfoID({ floor(startPoint.x + floor(playerPos.x / 64)), ceil((startPoint.y + floor(playerPos.y / 64))) + ceil(1 * player->getGoSpeed()) }) != 1 &&
					map->getCoordInfoID({ ceil(startPoint.x + floor(playerPos.x / 64)), ceil((startPoint.y + floor(playerPos.y / 64))) + ceil(1 * player->getGoSpeed()) }) != 1)
					startPoint.y += 1 * player->getGoSpeed();
				else
					startPoint.y = ceil(startPoint.y);
			} 

			if (key[CTRL_LEFT]){ //
				if (map->getCoordInfoID({ floor((startPoint.x + floor(playerPos.x / 64))) - ceil(1 * player->getGoSpeed()), floor(startPoint.y + floor(playerPos.y / 64)) }) != 1 &&
					map->getCoordInfoID({ floor((startPoint.x + floor(playerPos.x / 64))) - ceil(1 * player->getGoSpeed()), ceil(startPoint.y + floor(playerPos.y / 64)) }) != 1)
					startPoint.x -= 1 * player->getGoSpeed();
				else
					startPoint.x = floor(startPoint.x);
			}
			else if (key[CTRL_RIGHT]){
				if (map->getCoordInfoID({ ceil((startPoint.x + floor(playerPos.x / 64))) + ceil(1 * player->getGoSpeed()), floor(startPoint.y + floor(playerPos.y / 64)) }) != 1 &&
					map->getCoordInfoID({ ceil((startPoint.x + floor(playerPos.x / 64))) + ceil(1 * player->getGoSpeed()), ceil(startPoint.y + floor(playerPos.y / 64)) }) != 1)
					startPoint.x += 1 * player->getGoSpeed();
				else
					startPoint.x = ceil(startPoint.x);
			}
			redraw = true;
		}
		else if (ev.mouse.type == ALLEGRO_EVENT_MOUSE_AXES){
			mousePos = { ev.mouse.x, ev.mouse.y };
			playerAngel = std::atan((mousePos.x - playerPos.x) / (playerPos.y - mousePos.y));
			if (playerPos.y - mousePos.y < 0){
				playerAngel += 3.14159265;
			}
		}
		else if (ev.keyboard.type == ALLEGRO_EVENT_KEY_DOWN){
			if (ev.keyboard.keycode == ALLEGRO_KEY_W){
				key[CTRL_UP] = true;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_S){
				key[CTRL_DOWN] = true;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_A){
				key[CTRL_LEFT] = true;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_D){
				key[CTRL_RIGHT] = true;
			}
		}
		else if (ev.keyboard.type == ALLEGRO_EVENT_KEY_UP){
			if (ev.keyboard.keycode == ALLEGRO_KEY_W){
				key[CTRL_UP] = false;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_S){
				key[CTRL_DOWN] = false;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_A){
				key[CTRL_LEFT] = false;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_D){
				key[CTRL_RIGHT] = false;
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE){
				break;
			}
		}
		if (spawnNewBots == true){ //Span new Bots into Map
			for (unsigned i = 0; i < map->spawnPoints.size(); i++){
				bots->add((float)map->spawnPoints[i].x, (float) map->spawnPoints[i].y);
			}
			bots->findPath(0, { (startPoint.x + floor(playerPos.x / 64)), (startPoint.y + floor(playerPos.y / 64)) });
			bots->findPath(1, { (startPoint.x + floor(playerPos.x / 64)), (startPoint.y + floor(playerPos.y / 64)) });
			spawnNewBots = false;
		}
		al_flip_display();
	}
}